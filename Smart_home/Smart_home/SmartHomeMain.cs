﻿using Smart_home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Security.Cryptography;
using System.Data.Entity;
using System.Data.Entity.Validation;
using static Smart_home.Models.UserLoginRegister;

namespace Smart_home
{
    public class SmartHomeMain
    {
        static Guid idUser ;
        public void add(string name, string login, string password, string address, string nameEquipment,string role)
        {
            Guid equ = idUser;
            User user = null;
            using (var db = new SmartHomeContext())
            {

                User u = db.Users.Find(login);
                if (u != null)
                {
                    //Home homes = db.Homes.Where(h => h.User.Id == u.Id).FirstOrDefault();
                    IEnumerable<Home> homes = db.Homes.Where(h => h.User.Id == idUser).ToList();
                    foreach (Home h in homes)
                    {
                        equ = h.Id;
                        db.Homes.Remove(h);
                    }
                    IEnumerable<Equipment> equipments = db.Equipments.Where(e => e.HomeId == equ).ToList();
                    foreach (Equipment e in equipments)
                    {
                        db.Equipments.Remove(e);
                    }
                    db.Users.Remove(u);
                    db.SaveChanges();

                    password = GetHash(password);
                    user = new User { Name = name, Login = login, Password = password,Role=role };
                    var home = new Home { Address = address, UserHome= name };
                    var equipment = new Equipment { Name = nameEquipment, Status = false };
                    var log = new Log { message = DateTime.Now.ToString()+" Craete user" };
                    db.Users.Add(user);
                    db.Homes.Add(home);
                    db.Logs.Add(log);
                    db.Equipments.Add(equipment);



                    db.SaveChanges();


                }
            }
        }

              






        public void Remove(string login)
        {
            Guid equ = idUser; 
            using (SmartHomeContext db = new SmartHomeContext())
            {
                User u = db.Users.Find(login);
                if (u != null)
                {
                    //Home homes = db.Homes.Where(h => h.User.Id == u.Id).FirstOrDefault();
                    IEnumerable<Home> homes = db.Homes.Where(h => h.User.Id == idUser).ToList();
                    foreach (Home h in homes)
                    {
                        equ = h.Id;
                        db.Homes.Remove(h);
                    }
                    IEnumerable<Equipment> equipments = db.Equipments.Where(e => e.HomeId == equ).ToList();
                    foreach (Equipment e in equipments)
                    {
                        db.Equipments.Remove(e);
                    }
                    db.Users.Remove(u);
                    db.SaveChanges();
                }
            }
        }

        public string GetHash(string s)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(s);
            MD5CryptoServiceProvider CSP = new MD5CryptoServiceProvider();

            byte[] hashBytes = CSP.ComputeHash(bytes);
            string hash = string.Empty;
            foreach (byte b in hashBytes)
                hash += string.Format("{0:x2}", b);
            return hash;
        }
 
    }
}