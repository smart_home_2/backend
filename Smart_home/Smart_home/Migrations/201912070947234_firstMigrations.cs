namespace Smart_home.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstMigrations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Status = c.Boolean(nullable: false),
                        HomeId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Homes", t => t.HomeId, cascadeDelete: true)
                .Index(t => t.HomeId);
            
            CreateTable(
                "dbo.Homes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Address = c.String(nullable: false),
                        UserHome = c.String(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Role = c.String(nullable: false),
                        Login = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        message = c.String(),
                        EquipmentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipments", t => t.EquipmentId, cascadeDelete: true)
                .Index(t => t.EquipmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Logs", "EquipmentId", "dbo.Equipments");
            DropForeignKey("dbo.Homes", "UserId", "dbo.Users");
            DropForeignKey("dbo.Equipments", "HomeId", "dbo.Homes");
            DropIndex("dbo.Logs", new[] { "EquipmentId" });
            DropIndex("dbo.Homes", new[] { "UserId" });
            DropIndex("dbo.Equipments", new[] { "HomeId" });
            DropTable("dbo.Logs");
            DropTable("dbo.Users");
            DropTable("dbo.Homes");
            DropTable("dbo.Equipments");
        }
    }
}
