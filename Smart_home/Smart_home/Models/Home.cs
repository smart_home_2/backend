﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Smart_home.Models
{
    public class Home
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] public string Address { get; set; }
        [Required] public string UserHome{ get; set; }

        [Required] public Guid UserId { get; set; }
   
        public virtual User User { get; set; }
        public virtual ICollection<Equipment> Equipments { get; set; }

    }
}