﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Smart_home.Models
{
    public class Log
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
         public string message { get; set; }
        [Required] public Guid EquipmentId { get; set; }



        public virtual Equipment Equipment { get; set; }

    }
}