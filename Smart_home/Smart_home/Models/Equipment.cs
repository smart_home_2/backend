﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Smart_home.Models
{
    public class Equipment
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] public string Name { get; set; }
        [Required] public bool Status { get; set; }
        [Required] public Guid HomeId { get; set; }
        
        
        public virtual Home Home { get; set; }
        public virtual ICollection<Log> Logs { get; set; }

    }
}