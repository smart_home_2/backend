﻿using Smart_home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Smart_home.Controllers
{
    public class ArduinoController : ApiController
    {
        bool tmp;

        [HttpGet]
        public String status(int status,Guid equ) //с оборудованиея
        {
            if (status == 1)
            {
                tmp = true;
            }
            else
            {
                tmp = false;
            }
            using (SmartHomeContext db = new SmartHomeContext())
            {
                IEnumerable<Equipment> equipments = db.Equipments.Where(e => e.HomeId == equ).ToList();
                foreach (Equipment e in equipments)
                {
                    e.Status = tmp;
					
                }
                var log = new Log { message = DateTime.Now.ToString() + " Status " + tmp.ToString() };


                db.SaveChanges();
            }
            return tmp.ToString();

        }



        static Guid idUser;
        [HttpGet]
        public String Eqtmns(int status, string login) //на оборудование
        {
            if (status == 1)
            {
                tmp = true;
            }
            else
            {
                tmp = false;
            }
            var equipment= new Equipment();
            //IEnumerable<Home> homes;
            using (SmartHomeContext db = new SmartHomeContext())
            {
                User user = db.Users.FirstOrDefault(u => u.Login == login);
                idUser = user.Id;
                Guid equ = idUser;
                //IEnumerable<Home> homes;
                IEnumerable<Home> homes = db.Homes.Where(h => h.User.Id == idUser).ToList();
                foreach (Home h in homes)
                {
                    equ = h.Id;
                }
                IEnumerable<Equipment> equipments = db.Equipments.Where(e => e.HomeId == equ).ToList();
                foreach (Equipment e in equipments)
                {
                    e.Status = tmp;
                }
                var log = new Log { message = DateTime.Now.ToString() + " Status " + tmp.ToString() };

                
                db.SaveChanges();



            }



            return "OK";

        }

    }


}
