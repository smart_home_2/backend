﻿using Smart_home.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using static Smart_home.Models.UserLoginRegister;

namespace Smart_home.Controllers.API
{
    
    public class DesktopController : ApiController
    {
        static Guid idUser;
        SmartHomeMain smartMain = new SmartHomeMain();

        [Authorize(Roles = "Admin")]
        [HttpGet] public String Add (string name, string login, string password, string address, string nameEquipment, string role)
        {
           
                try
                {
                smartMain.add(name, login, password, address, nameEquipment, role);
                    return ("ОК");
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (DbEntityValidationResult validationError in ex.EntityValidationErrors)
                    {
                        foreach (DbValidationError err in validationError.ValidationErrors)
                        {
                            return ("Object: " + validationError.Entry.Entity.ToString()+"|||"+err.ErrorMessage + " ");
                        }
                    }
                }

            return ("Неопознано");

        }


        
        
       [HttpGet] public void Edit(string name, string login, string password, string address, string userHome, string nameEquipment, bool status)
       {
            //smartMain.Edit(name, login, password, address, userHome, nameEquipment, status);
            
       }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public void Remove(string login)
        {
            smartMain.Remove(login);
            
        }

    }
}
