﻿using Smart_home.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Security;
using static Smart_home.Models.UserLoginRegister;

namespace Smart_home.Controllers
{
    public class HomeController : Controller
    {
        SmartHomeContext smartContext=new SmartHomeContext();
        SmartHomeMain smartMain = new SmartHomeMain();
        static Guid equ;
        static Guid idUser;
      
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin model)
        {
            if (ModelState.IsValid)
            {


                User user = null;
                using (SmartHomeContext db = new SmartHomeContext())
                {
                   
                   
                    string password = smartMain.GetHash(model.Password);
                    user = db.Users.FirstOrDefault(u => u.Login == model.Login && u.Password == password);

                    try
                    {
                        idUser = user.Id;
                    }
                    catch
                    {

                    }
                }
                if (user != null)
                {
                    var ticet = new FormsAuthenticationTicket(1, user.Login, DateTime.Now, DateTime.Now.AddDays(1), true, user.Role);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticet));
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("Show", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Пользователя с таким логином и паролем нет");
                }
            }

            //return View(model);
            return View(User);
        }


        public ActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserRegister model)
        {
            if (ModelState.IsValid)
            {
                User user = null;
                using (SmartHomeContext db = new SmartHomeContext())
                {
                    user = db.Users.FirstOrDefault(u => u.Login == model.Login);
                }
                if (user == null)
                {
                    using (SmartHomeContext db = new SmartHomeContext())
                    {
                        string password = smartMain.GetHash(model.Password);
                        //smartMain.add(name, login, password, address, nameEquipment, status);
                        db.Users.Add(new User { Name = model.Name, Password = password, Login = model.Login, Role="User" });
                        db.Homes.Add(new Home { Address = model.Address, UserHome = model.Name });
                        db.Equipments.Add(new Equipment {Name=model.EquipmentName,Status=false });
                        db.Logs.Add(new Log { message = DateTime.Now.ToString() + " Craete user" });
 
                         db.SaveChanges();
                        user = db.Users.Where(u => u.Login == model.Login && u.Password == password).FirstOrDefault();
                    }
                    if (user != null)
                    {
                        // FormsAuthentication.SetAuthCookie(user.Login, true);
                        var ticet = new FormsAuthenticationTicket(1, user.Login, DateTime.Now, DateTime.Now.AddDays(1), true, user.Role);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName,FormsAuthentication.Encrypt(ticet));
                        cookie.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(cookie);

                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Пользователь с таким логином уже существует");
                }
                
            }

            return View(model); 
        }
    

        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");

        }

        [Authorize(Roles = "Admin,User")]
        public ActionResult Show()
        {

            //IEnumerable<Home> homes;
            using (SmartHomeContext db = new SmartHomeContext())
            {
                Guid equ= idUser;
                //IEnumerable<Home> homes;
                IEnumerable<Home> homes = db.Homes.Where(h => h.User.Id == idUser).ToList();
                foreach (Home h in homes)
                {
                    equ = h.Id;
                }
                IEnumerable<Equipment> equipments = db.Equipments.Where(e => e.HomeId == equ).ToList();
                
                    ViewBag.Homes = homes;
                    ViewBag.Equipments = equipments;

            }
            return View();
        }
    }
}